#include "track.h"
#include <iostream>
#include <fstream>

using namespace jsoncons;


track::track(const json& data) {
	const json& lanes = data["race"]["track"]["lanes"];
	_total_laps = data["race"]["raceSession"]["laps"].as<size_t>();

	std::vector<double> lane_offsets;
	for(auto it = lanes.begin_elements(); it != lanes.end_elements(); ++it) {
		lane_offsets.push_back( (*it)["distanceFromCenter"].as<double>() );
	}

	std::vector<double> global_lane_distance;
	for(int i=0; i<lanes.size(); ++i) {
		global_lane_distance.push_back( 0.0 );
	}

	const json& pieces = data["race"]["track"]["pieces"];
	int i_piece = 0;
	for(auto it = pieces.begin_elements(); it != pieces.end_elements(); ++it,++i_piece) {

		std::vector<track_segment*> v_lanes;

		const json& piece = *it;
		bool is_switch = piece.has_member("switch") && piece["switch"].as<bool>();
		int i_lane = 0;
		if(piece.has_member("length")) { // is straight
			double length = piece["length"].as<double>();
			for(auto lane_it = lane_offsets.begin(); lane_it != lane_offsets.end(); ++lane_it,++i_lane) {
				v_lanes.push_back( new track_segment(i_piece, i_lane, 0.0, length, global_lane_distance[i_lane], is_switch) );
				global_lane_distance[i_lane] += length;
			}
		} else { // is turn
			double radius = piece["radius"].as<double>();
			double angle = piece["angle"].as<double>();
			double angle_sign = 1.0;
			if(angle < 0.0) {
				angle_sign = -1.0;
				angle *= -1.0;
			}
			for(auto lane_it = lane_offsets.begin(); lane_it != lane_offsets.end(); ++lane_it,++i_lane) {
				double lane_offset = *lane_it;
				double arc_length = (M_PI / 180.0) * (radius - (angle_sign * lane_offset)) * angle;
				v_lanes.push_back( new track_segment(i_piece, i_lane,  1.0 / (radius - (angle_sign * lane_offset)),
							arc_length, global_lane_distance[i_lane], is_switch) );
				global_lane_distance[i_lane] += arc_length;
			}
		}

		_segment_by_piece_by_lane.push_back( v_lanes );
	}

	for(int i=0; i<lanes.size(); ++i) _lap_length.push_back(global_lane_distance[i]);

}

const track_segment * track::get_segment(size_t piece, size_t lane) const {
	return _segment_by_piece_by_lane[piece][lane];
}

size_t track::get_num_pieces() const {
	return _segment_by_piece_by_lane.size();
}

void track::log_data_as_csv(std::string filename) const {
	std::ofstream track_file;
	track_file.open(filename);
	track_file << "Piece,Lane,Length,Curvature,Switch\n";
	for(auto it_pieces=_segment_by_piece_by_lane.begin(); it_pieces!=_segment_by_piece_by_lane.end(); ++it_pieces) {
		std::vector<track_segment*> lanes = *it_pieces;
		for(auto it_lanes=it_pieces->begin(); it_lanes != it_pieces->end(); ++it_lanes) {
			const track_segment *segment = *it_lanes;
			track_file
				<< segment->piece << ","
				<< segment->lane << ","
				<< segment->length << ","
				<< segment->curvature << ","
				<< segment->is_switch << std::endl;
		}
	}
	track_file.close();
}

double track::get_global_position(size_t piece, double in_piece_distance, size_t lane, size_t lap) const {
	return _segment_by_piece_by_lane[piece][lane]->segment_start + in_piece_distance + _lap_length[lane] * lap;
}

