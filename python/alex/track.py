import math

class Piece():
    def __init__(self, lanes, length, switch=False, radius=None, angle=None):
        self.length = length
        self.lanes = lanes
        self.switch = switch
        self.angle = math.radians(angle) if angle is not None else None
        self.radius = radius
        self.max_pieces = 0

    def get_distance(self, lane, switch=None):
        if self.radius is None:
            if switch is None:
                return self.length
            else:
               # return math.sqrt(self.length*self.length + math.pow(self.lanes[lane]['distanceFromCenter']-self.lanes[lane+switch]['distanceFromCenter'],2))
               return self.length
        else:
            angleMultp = math.copysign(1.0, self.angle)
            if switch is not None:
                from_r = self.radius + self.lanes[lane]['distanceFromCenter']
                from_x = math.cos(math.pi*2-self.angle)*from_r
                from_y = math.sin(math.pi*2-self.angle)*from_r
                to_x = 0
                to_y = self.radius+(self.lanes[lane+switch]['distanceFromCenter']*angleMultp)
                return math.sqrt(math.pow(to_x-from_x,2)+math.pow(to_y-from_y,2))
            else:
                if len(self.lanes)<lane:
                    print self.lanes, lane
                return 2.0*math.pi*(self.radius-(angleMultp*self.lanes[lane]['distanceFromCenter']))*self.angle/(2*math.pi)*angleMultp
    def __repr__(self):
        return 'length='+str(self.length) + ' radius=' + str(self.radius) +' angle='+ str(self.angle)

    def get_radius(self, lane):
        angleMultp = math.copysign(1.0, self.angle)
        return self.radius - (angleMultp*self.lanes[lane]['distanceFromCenter'])


class Track():
    
    def __init__(self, data):
        lanes = data['race']['track']['lanes']
        self.lanes = lanes
        track = []
        current_curve_radius = 0
        simple_track = []
        current_piece = None
        sign = lambda x: math.copysign(1, x)
        i = 0
        for piece in data['race']['track']['pieces']:
            p = Piece(lanes, piece['length'] if 'length' in piece else None, switch= 'switch' in piece and piece['switch'], radius= piece['radius'] if 'radius' in piece else None, angle= piece['angle'] if 'angle' in piece else None)
            track.append(p)
            if current_piece is None:
                current_piece = Piece(lanes, piece['length'] if 'length' in piece else None, switch= 'switch' in piece and piece['switch'], radius= piece['radius'] if 'radius' in piece else None, angle= piece['angle'] if 'angle' in piece else None)
                current_piece.start = i
            else:
                if current_piece.radius is None and p.radius is None:
                    current_piece.length+= p.length
                elif current_piece.radius == p.radius and sign(current_piece.angle)==sign(p.angle):
                    current_piece.angle+=p.angle
                else:
                    simple_track.append(current_piece)
                    current_piece.end=i
                    current_piece = Piece(lanes, piece['length'] if 'length' in piece else None, switch= 'switch' in piece and piece['switch'], radius= piece['radius'] if 'radius' in piece else None, angle= piece['angle'] if 'angle' in piece else None)
                    current_piece.start=i
            i+=1
        simple_track.append(current_piece)
        current_piece.end=i
        self.simple_track  = simple_track
        self.track_pieces = track
        self.max_pieces = len(track)
        for lane in range(len(lanes)):
            dist = 0
            i = 0
            for piece in track:
                dist+=piece.get_distance(lane)
                i=i+1
        self.cars = {x['id']['color']:{'data':[], 'last_pos':0, 'total_dist':0, 'lap':0} for x in data['race']['cars']}
        self.last_tick = 0
        self.vconstant = None
        self.path = None
        self.laps = data['race']['raceSession']['laps']
    
    def update_poss(self, data, tick):
        tick_d = tick - self.last_tick 
        self.last_tick = tick
        for car in data:
            stored_car = self.cars[car['id']['color']]
            onPiece = car['piecePosition']['pieceIndex']
            onPieceDist = car['piecePosition']['inPieceDistance']
            stored_car['piece_dist']=onPieceDist
            inLane = car['piecePosition']['lane']['endLaneIndex']
            stored_car['in_lane']=inLane
            angle = car['angle']
            if stored_car['last_pos']!=onPiece:
                stored_car['total_dist']+=self.track_pieces[stored_car['last_pos']].get_distance(inLane)
                stored_car['last_pos']=onPiece
            piece_total_distance = stored_car['total_dist']+onPieceDist
            if len(stored_car['data'])>0:
                if tick_d == 0 :
                    return
                last_data=stored_car['data'][-1]
                v = (piece_total_distance - last_data[0])/tick_d
                a = 0 if len(stored_car['data'])<2 else v-stored_car['data'][-1][1]
                da = (angle - last_data[3])/tick_d
                stored_car['data'].append((piece_total_distance, v, a, angle, da))
            else:
                stored_car['data'].append((piece_total_distance, 0, 0, angle, 0))
    
    def car_finished(self, car_color):
        self.cars[car_color]['lap']+=1
        print self.cars[car_color]['lap']

    def get_nextv(self, ticks, startv):
        constant = (startv +(self.inital_a/self.vconstant))/math.exp(self.vconstant)
        return constant*math.exp(self.vconstant*ticks)-(self.inital_a/self.vconstant - self.inital_a)

    def updatePath(self):
        self.path = self.get_path(0,self.cars[self.car_color]['in_lane'])[::-1]

    def getSimpleIndex(self, track_pos):
        for i in range(len(self.simple_track)):
            if self.simple_track[i].start<=track_pos and self.simple_track[i].end>track_pos:
                return i

    def getNextCurves(self, car_color):
        curves = []
        car = self.cars[car_color]
        current_pos = car['last_pos']
        current_piece = self.track_pieces[current_pos]
        dist_to_curve = 0
        current_lane = car['in_lane']
        simple_i=None
        if current_piece.radius is None:
            dist_to_curve = current_piece.length - car['piece_dist']
            i=1
            while self.track_pieces[(current_pos+i)%(len(self.track_pieces)-1)].radius is None:
                dist_to_curve += self.track_pieces[(current_pos+i)%(len(self.track_pieces)-1)].length
                i+=1
            simple_i = self.getSimpleIndex(current_pos)
        else:
            simple_i = self.getSimpleIndex(current_pos)
            curve = self.simple_track[simple_i]
            curves.append((0,curve.get_radius(current_lane), curve.angle))
            dist_to_curve = current_piece.get_distance(current_lane) - car['piece_dist']
            for i in (current_pos+1, curve.end+1):
                dist_to_curve += self.track_pieces[i%(len(self.track_pieces)-1)].get_distance(current_lane)
        while(dist_to_curve < 800):
            simple_i = (simple_i+1)%(len(self.simple_track)-1)
            simple_p = self.simple_track[simple_i]
            if simple_p.radius is None:
                dist_to_curve+=simple_p.length
            else:
                curves.append((dist_to_curve, simple_p.get_radius(current_lane), simple_p.angle))
                dist_to_curve+=simple_p.get_distance(current_lane)
        return curves
    
    def turboAvail(self, data):
        pos = self.cars[self.car_color]['last_pos']
        max_length = 0
        max_piece = None
        prev_p = self.simple_track[-1]
        if prev_p.radius is None and self.simple_track[0].radius is None:
            max_piece = prev_p
            max_length = prev_p.length+self.simple_track[0].length

        for i in range(len(self.simple_track)):
            sp = self.simple_track[i]
            if sp.radius is None:
                if sp.length>=max_length:
                    max_length = sp.length
                    max_piece = sp
        self.path[max_piece.start]['turbo']='blah'
                

    def get_next_pos(self):
        current_data = self.cars[self.car_color]['data'][-2:]
        print current_data[-1]
        if len(self.cars[self.car_color]['data'])>5 and self.vconstant is None:
            if current_data[1][1]>0:
                vconstant = (current_data[0][2] - current_data[1][2] ) / (current_data[0][1] - current_data[1][1])
                if math.fabs(vconstant)<.5:
                    for row in self.cars[self.car_color]['data']:
                        if row[2]>0:
                            self.inital_a = row[2]
                            break
                    self.vconstant = vconstant
                    self.reached_speed = False
        if self.vconstant is None:
            return 'throttle',1

        path_pl = self.path[(self.cars[self.car_color]['last_pos']+1)%(len(self.path)-1)]
        if path_pl['switch']!=0:
            ret = 'switchLane', 'Right' if path_pl['switch']>0 else 'Left'
            path_pl['switch']=0
            return ret

        if 'turbo' in path_pl:
            del path_pl['turbo']
            return 'turbo','chug a chuga'

        lap = self.cars[self.car_color]['lap']
        if lap+1==self.laps:
            pp = self.cars[self.car_color]['last_pos']
            sp = self.getSimpleIndex(pp)
            if sp==len(self.simple_track)-1:
                return 'throttle', 1 

        dist, v, a, angle, dangle = self.cars[self.car_color]['data'][-1]
        next_curves = self.getNextCurves(self.car_color)
        throttle = 1
        for curve in next_curves:
            next_curve_dist, radius, angle = curve
            angle = math.fabs(angle)
            max_v = max((radius/(math.degrees(angle)))*3.5,9)
            if next_curve_dist == 0:
                if math.fabs(max_v-v)<.2:
                    throttle = max_v/10
                elif max_v>v:
                    throttle = 1
                else:
                    throttle = 0
            elif v > max_v:
                ticks_till_v = math.log(max_v/v)/self.vconstant
                distance = v*ticks_till_v
                if distance>next_curve_dist:
                    throttle = 0
        return 'throttle',max(0,min(throttle,1))

    def get_sum(self, path):
        ret = 0
        for loc in path:
            ret += loc['distance']
        return ret
        
    def get_path(self, current_p, current_lane):
        if current_p >= self.max_pieces:
            return []
        piece = self.track_pieces[current_p]
        same_path = self.get_path(current_p+1, current_lane)
        same_d = self.get_sum(same_path)
        piece_d = piece.get_distance(current_lane)
        p_type = 's' if piece.radius is None else 'c'
        if piece.switch:
            if current_lane == 0 and len(self.lanes)>1:
                path = self.get_path(current_p+1, current_lane+1)
                distance_c = piece.get_distance(current_lane, 1)
                path_d = self.get_sum(path)
                if path_d+distance_c < piece_d+same_d:
                    path.append({'distance':distance_c,'lane':current_lane, 'switch': 1, 'type':p_type})
                    return path
                else:
                    same_path.append({'distance':piece_d,'lane':current_lane, 'switch': 0, 'type':p_type})
                    return same_path
            elif current_lane > 0 and current_lane < len(self.lanes)-1:
                up_path = self.get_path(current_p+1, current_lane+1) 
                down_path = self.get_path(current_p-1, current_lane-1) 
                up_d = self.get_sum(up_path)
                down_d = self.get_sum(down_path)
                up_s_d = piece.get_distance(current_lane, 1)
                down_s_d = piece.get_distance(current_lane, -1)
                if piece_d+same_d < up_d+up_s_d and piece_d+same_d< down_d+down_s_d:
                    same_path.append({'distance':piece_d,'lane':current_lane, 'switch': 0, 'type':p_type})
                    return same_path
                elif piece_d+same_d > up_d+up_s_d and up_d+up_s_d< down_d+down_s_d:
                    up_path.append({'distance':up_s_d,'lane':current_lane, 'switch': 1, 'type':p_type})
                    return up_path
                elif piece_d+same_d > down_d+down_s_d and up_d+up_s_d< down_d+down_s_d:
                    down_path.append({'distance':down_s_d,'lane':current_lane, 'switch': -1, 'type':p_type})
                    return down_path
                    

            elif current_lane>0:
                path = self.get_path(current_p+1, current_lane-1)
                distance_c = piece.get_distance(current_lane, -1)
                path_d = self.get_sum(path)
                if path_d+distance_c < piece_d+same_d:
                    path.append({'distance':distance_c,'lane':current_lane, 'switch': -1,'type':p_type})
                    return path
                else:
                    same_path.append({'distance':piece_d,'lane':current_lane, 'switch': 0,'type':p_type})
                    return same_path
        same_path.append({'distance':piece_d,'lane':current_lane, 'switch': 0,'type':p_type})
        return same_path
