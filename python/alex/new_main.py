import json
import socket
import sys
from mimic_track import Track


class NoobBot(object):

    def __init__(self, socket, name, key, command=None, trackname=None, password=None, carcount=None):
        self.socket = socket
        self.name = name
        self.key = key
        self.turbo_available = False
        self.command=command
        self.trackname = trackname
        self.password = password
        self.carcount = carcount
        self.tick = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        if self.command is not None and self.command == "join":
            print "JOIN"
            return self.msg("joinRace", {'botId':{"name": self.name, "key": self.key},'trackName':self.trackname,'password':self.password,'carCount':self.carcount})
        elif command is not None and command == "create":
            print "CREATE"
            return self.msg("createRace", {'botId':{"name": self.name, "key": self.key},'trackName':self.trackname,'password':self.password,'carCount':int(self.carcount)})
        else:
            print "JOIN QUICKRACE"
            return self.msg("join", {"name": self.name, "key": self.key})


    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")

    def on_game_start(self, data):
        self.throttle(1)
        self.race_started = True

    def on_car_positions(self, data):
        self.track.update_poss(data, self.tick)
        if self.track.path is None:
            self.track.updatePath()
        self.msg(*self.track.get_next_pos())
        

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def gameInit(self, data):
        self.track = Track(data)
        self.track.car_color = self.car_color
        
    def setCar(self, data):
        self.car_color = data['color']

    def lapReset(self, data):
        self.track.car_finished(data['car']['color'])
        self.track.updatePath()

    def turboAvailable(self, data):
        self.track.turboAvail(data)
        self.turbo_available = True

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.gameInit,
            'yourCar': self.setCar,
            'lapFinished' : self.lapReset,
            'turboAvailable' : self.turboAvailable,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.tick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) < 5 or len(sys.argv) == 6 or len(sys.argv) == 7 or len(sys.argv) > 9:
        print("Usage: ./run host port botname botkey [create|join trackname password carcount]")
    else:
        host, port, name, key = sys.argv[1:5]
        command, trackname, password, carcount = [None, None, None, None]
        if len(sys.argv) == 9:
            command, trackname, password, carcount = sys.argv[5:9]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key, command, trackname, password, carcount)
        bot.run()
