import math
from numerical import solve_2nd_ode_for_x

def lerp(a, b, alpha):
    return alpha*b + (1-alpha)*a

def lerp_fn(fn, a, b, x):
    if x == a: return fn[0] 
    if x == b: return fn[len(fn)-1]
    if x < a or x > b: return None 

    step = (b - a) / (len(fn)-1)
    left = int(math.floor((x - a) / step))
    alpha = ((x - a) - left*step) / step
    return lerp(fn[left], fn[left+1], alpha)

class Car(object):

    def __init__(self, name, color, dimensions, track):
        self.name = name
        self.color = color
        self.v = 0.0 #velocity (dx/dt)
        self.a = 0.0 #acceleration (dv/dt)
        self.t = 0 #current tick
        self.angle = 0.0
        self.d_angle = 0.0
        self.i_piece = len(track.pieces[0]) - 1
        self.x_piece = 0.0 #position in piece
        self.start_lane = None
        self.end_lane = None
        self.lap = 0
        self.dim_length = dimensions['length']
        self.dim_width = dimensions['width']
        self.dim_x_flag = dimensions['guideFlagPosition']
        self.lost_dt = 0.0 #when time passes that we have to skip xva updates
        self.turbo_ticks = 0
        self.turbo_is_active = False
        self.turbo_factor = 1.0
        self.turbo_ticks_available = 0
        self.switch_direction = None
        self.track = track
        self.throttle_solver = ThrottleSolver(self)

    def update_vitals(self, car_data, t, dt):
        piece_data = car_data['piecePosition']
        new_i_piece = piece_data['pieceIndex']
        prev_x_piece = self.x_piece
        self.x_piece = piece_data['inPieceDistance']
        self.start_lane = piece_data['lane']['startLaneIndex']
        self.end_lane = piece_data['lane']['endLaneIndex']
        self.lap = piece_data['lap']
        self.t = t

        if self.turbo_is_active:
            if self.turbo_expiration <= t:
                self.turbo_is_active = False

        if dt > 0.0:
            new_angle = car_data['angle']
            new_d_angle = (new_angle - self.angle) / dt
            self.angle = new_angle
            self.d_angle = new_d_angle

            if new_i_piece == self.i_piece: # only calc v and a when we can (not on piece borders)
                new_v = (self.x_piece - prev_x_piece) / dt
                new_a = (new_v - self.v) / (dt + self.lost_dt)
                self.lost_dt = 0.0
                self.v = new_v
                self.a = new_a
            else:
                self.lost_dt += dt

        if new_i_piece != self.i_piece:
            i_piece_until_switch = self.track.get_i_pieces_until_switch(new_i_piece)
            self.switch_direction = None
            if i_piece_until_switch == 1:
                ideal_lane = self.track.get_ideal_lane(new_i_piece+1)
                if ideal_lane < self.end_lane:
                    self.switch_direction = "Left"
                elif ideal_lane > self.end_lane:
                    self.switch_direction = "Right"

        self.i_piece = new_i_piece

    def get_throttle(self):
        if abs(self.angle + self.d_angle) > 55.0: return 0.0

        ticks_till_fall = self.throttle_solver.solve(1000, 30.0*self.v, self.v, self.turbo_is_active)
        
        if ticks_till_fall is not None:
            return 0.0
        else:
            return 1.0

    def enable_turbo(self, ticks, factor):
        self.turbo_ticks_available = ticks
        self.turbo_factor = factor

    def is_turbo_activatable(self):
        if self.turbo_is_active or self.turbo_ticks_available == 0:
            return False

        if abs(self.angle + self.d_angle) > 55.0: return False

        curvature = self.track.sample_curvature(100, 10.0*self.dim_length, self.i_piece, self.x_piece, self.start_lane)

        for c in curvature:
            if c > 0.0:
                return False

        return True

    def activate_turbo(self):
        self.turbo_is_active = True
        self.turbo_expiration = self.t + self.turbo_ticks_available
        self.turbo_ticks_available = 0

    def wants_switch(self):
        return self.switch_direction != None

    def get_switch_direction(self):
        switch_direction = self.switch_direction
        self.switch_direction = None
        return switch_direction

    def get_sample_width(self):
        return self.dim_length*self.v*self.get_turbo_factor()

    def get_turbo_factor(self):
        return self.turbo_factor if self.turbo_is_active else 1.0

class ThrottleSolver(object):

    def __init__(self, car):
        self.car = car
        self.sample_log = open('ThrottleSolver.csv', 'w')
        self.sample_log.write('t,f,f_p,f_p_p\n')
        self.i_sample = 0

        #need to adjust these
        self.k_s = 0.5
        self.k_damp = 1.0
        self.k_curv = 100.0

    def solve(self, n, distance, v_0, log=False):
        self.c = self.car.track.sample_curvature(n, distance, self.car.i_piece, self.car.x_piece, self.car.start_lane)
        self.i_sample = 0
        self.theta = self.car.angle
        self.d_theta = self.car.d_angle
        self.v = v_0
        max_abs_theta = 55.0
        return solve_2nd_ode_for_x(self.theta, self.d_theta, self.angle_f_p_p, self.on_update, 0.0, distance, n, max_abs_theta, True, self.sample_log if log else None)

    def angle_f_p_p(self):
        f_p_p = -self.k_s*self.theta - self.k_damp*self.d_theta + self.k_curv*self.v*self.v*self.c[self.i_sample]*math.cos(math.radians(self.theta))
        self.i_sample += 1
        return f_p_p

    def on_update(self, f_t, f_p_t):
        self.theta = f_t
        self.d_theta = f_p_t

