import math
from array import array

class Piece(object):

    def __init__(self, i, lane, curvature, length, x_start, is_switch, i_switch):
        self.i = i
        self.lane = lane
        self.curvature = curvature
        self.length = length
        self.x_start = x_start
        self.is_switch = is_switch
        self.i_switch = i_switch

class Track(object):

    def __init__(self, track_data, ideal_lane_config):
        self.pieces = [] # array of lanes, which are arrays of Piece
        self.ideal_lanes = {} # ideal lane mapped by i_piece

        i_lane = 0
        x_start = 0.0
        for lane in track_data['lanes']:
            lane_offset = lane['distanceFromCenter']
            i_piece = 0
            lane_segments = []
            i_switch = 0
            i_ideal_lane = 0
            for piece in track_data['pieces']:
                is_switch = piece['switch'] if piece.get('switch') else False
                if is_switch:
                    self.ideal_lanes[i_piece] = ideal_lane_config[i_ideal_lane]
                    i_ideal_lane += 1
                if piece.get('length'): #straight
                    curvature = 0.0
                    length = piece['length']
                    lane_segments.append( Piece( i_piece, i_lane, curvature, length, x_start, is_switch, i_switch) )
                    x_start += length
                    i_lane += 1
                else: #curve
                    lane_radius = (piece['radius'] - lane_offset)
                    curvature = abs(1.0 / lane_radius)
                    length = math.radians( piece['angle'] ) * lane_radius
                    lane_segments.append( Piece( i_piece, i_lane, curvature, length, x_start, is_switch, i_switch) )
                    x_start += length
                    i_lane += 1
                if is_switch:
                    i_switch += 1
                i_piece += 1
            self.pieces.append( lane_segments )
            i_lane += 1

    def sample_curvature(self, num_samples, sample_length, i_piece, x_piece, start_lane):
        track_samples = []
        step = sample_length / num_samples
        segment = self.pieces[start_lane][i_piece]
        current_lane = start_lane

        while len(track_samples) < num_samples:
            if x_piece >= segment.length:
                x_piece = x_piece + step - segment.length
                if segment.is_switch:
                    if self.ideal_lanes[segment.i] > current_lane:
                        current_lane += 1
                    elif self.ideal_lanes[segment.i] < current_lane:
                        current_lane -= 1
                i_piece = (i_piece+1) % len(self.pieces[0])
                segment = self.pieces[current_lane][i_piece]
            else:
                x_piece += step
            track_samples.append(segment.curvature)

        return track_samples

    def get_i_pieces_until_switch(self, current_i_piece):
        current_i_piece = (current_i_piece + 1) % len(self.pieces[0])
        i_pieces_until_switch = 1
        while i_pieces_until_switch < len(self.pieces[0]):
            if self.pieces[0][current_i_piece].is_switch:
                return i_pieces_until_switch
            i_pieces_until_switch += 1
            current_i_piece = (current_i_piece + 1) % len(self.pieces[0])
        return None
            
    def get_ideal_lane(self, i_piece):
        return self.ideal_lanes[i_piece % len(self.pieces[0])]

