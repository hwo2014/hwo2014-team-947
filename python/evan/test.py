import math

def lerp(a, b, alpha):
    return alpha*b + (1-alpha)*a

def lerp_fn(fn, a, b, x):
    if x == a: return fn[0] 
    if x == b: return fn[len(fn)-1]
    if x < a or x > b: return None 

    step = (b - a) / (len(fn)-1)
    left = int(math.floor((x - a) / step))
    alpha = ((x - a) - left*step) / step
    return lerp(fn[left], fn[left+1], alpha)

def lerp_fn_by_y(fn, a, b, y):
    step = (b - a) / (len(fn)-1)
    for i in range(len(fn)-1):
        if fn[i] < y and y < fn[i+1]:
            slope = (fn[i+1] - fn[i]) / step
            print i, fn[i], fn[i+1], slope
            return a + i * step + (y - fn[i]) / slope
    return None

sample_log = open('test.csv', 'w')
sample_log.write('t,f,f_p,f_p_p\n')
#   f_k | initial value of f
# f_p_k | initial value of f'
# f_p_p | function f''
# [a,b] | interval
#     n | number of samples >= 2
#     y | solve for value
def lerp_solve_for_y(f_k, f_p_k, f_p_p, f_u, a, b, n, y):
    step = (b - a) / (n-1)
    x_0 = None
    y_0 = None
    x_1 = a
    y_1 = f_k
    f_p_p_k = f_p_p()
    sample_log.write(','.join([str(0),str(f_k),str(f_p_k),str(f_p_p_k)]) + '\n')
    for i in range(1,n):
        f_k += step*f_p_k
        f_p_p_k = f_p_p()
        f_p_k += step*f_p_p_k
        f_u(f_k, f_p_k)
        sample_log.write(','.join([str(i*step),str(f_k),str(f_p_k),str(f_p_p_k)]) + '\n')
        x_0 = x_1
        y_0 = y_1
        x_1 += step
        y_1 = f_k
        if (y_0 > y and y > y_1) or (y_0 < y and y < y_1):
            slope = (y_1 - y_0) / step
            return x_0 + (y - y_0) / slope
        elif y == y_0: return x_0
        elif y == y_1: return x_1
    return None

theta = None
d_theta = None
C = 0.01111
v = None

def angle_f_t():
    global theta, d_theta, v, C
    k_s = 1.0
    k_damp = 0.5
    k_curv = 1.0
    return -k_s*theta - k_damp*d_theta + k_curv*v*v*C*math.cos(math.radians(theta))

def on_update(new_theta, new_d_theta):
    global theta, d_theta
    theta = new_theta
    d_theta = new_d_theta

t_samples = []
t_sample_log = open('t_sample.csv', 'w')
t_sample_log.write('i,t\n')
a = 0.0
for i in range(11):
    throttle = 0.1 * i
    v = (a - 0.2*throttle)/(-0.02)
    a = -0.02*v + 0.2*throttle
    theta = 55.0
    d_theta = 0.0
    sample = lerp_solve_for_y(theta, d_theta, angle_f_t, on_update, 0.0, 50.0, 200, 60.0)
    t_sample_log.write(','.join([str(i),str(sample)]) + '\n')
    if sample is not None: t_samples.append(sample)

x = 0
min_t = 99999999.0
min_x = None
for t in t_samples:
    if t < min_t:
        min_t = t
        min_x = x
    x += 1
if min_x:
    v = (a - 0.2*10.0*min_x)/(-0.02)
    print v
else:
    print 'No solution.'
