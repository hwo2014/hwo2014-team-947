#include "game_logic.h"
#include "protocol.h"
#include "track.h"
#include <string>

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "yourCar", &game_logic::on_your_car },
      { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error }
    }
{
	_config = jsoncons::json::parse_file("config.json");
	_throttle_scaler = _config["throttle_scaler"].as<double>();
	_curvature_scaler = _config["curvature_scaler"].as<double>();
	if(_config.has_member("msgs_log_file")) {
		_msg_log.reset( new std::ofstream() );
		_msg_log->open(_config["msgs_log_file"].as<std::string>());
	}
	if(_config.has_member("sim_log_file")) {
		_sim_log.reset( new std::ofstream() );
		_sim_log->open(_config["sim_log_file"].as<std::string>());
		(*_sim_log) << "Position,Angle,Throttle,Lane\n";
	}
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  if(_msg_log.get()) (*_msg_log) << msg << std::endl;
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
  std::cout << "yourCar" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  std::cout << "Game init" << std::endl;
  _track.reset( new track(data) );
  if(_config.has_member("track_data_file")) {
  	_track->log_data_as_csv( _config["track_data_file"].as<std::string>());
  }
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  size_t piece = (*data.begin_elements())["piecePosition"]["pieceIndex"].as<size_t>();
  piece = (piece + 2) % _track->get_num_pieces();
  size_t lane = (*data.begin_elements())["piecePosition"]["lane"]["startLaneIndex"].as<size_t>();
  double in_piece_distance = (*data.begin_elements())["piecePosition"]["inPieceDistance"].as<double>();
  size_t lap = (*data.begin_elements())["piecePosition"]["lap"].as<size_t>();
  double one_minus_c = 1.0 - _curvature_scaler * _track->get_segment(piece, lane)->curvature;
  double throttle = _throttle_scaler * one_minus_c;
  if(_sim_log.get()) {
  	(*_sim_log) << _track->get_global_position(piece, in_piece_distance, lane, lap) << ",";
  	(*_sim_log) << (*data.begin_elements())["angle"].as<double>() << ",";
  	(*_sim_log) << throttle << ",";
  	(*_sim_log) << lane << std::endl;
  }

  return { make_throttle(throttle) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}
