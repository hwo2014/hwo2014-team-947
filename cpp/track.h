#ifndef TRACK_H
#define TRACK_H

#include <vector>
#include <cstdlib>
#include <iostream>
#include <jsoncons/json.hpp>

struct track_segment {
	size_t piece;
	size_t lane;
	double curvature;
	double length;
	double segment_start;
	bool is_switch;
	track_segment(size_t piece, size_t lane, double curvature, double length, double segment_start, bool is_switch)
		: piece(piece),lane(lane),curvature(curvature),length(length),segment_start(segment_start),is_switch(is_switch) {}
};

class track {
	std::vector< std::vector<track_segment*> > _segment_by_piece_by_lane;
	std::vector<double> _lap_length;
	size_t _total_laps;

	public:
	track(const jsoncons::json& data);
	const track_segment * get_segment(size_t piece, size_t lane) const;
	size_t get_num_pieces() const;
	void log_data_as_csv(std::string filename) const;
	double get_global_position(size_t piece, double in_piece_distance, size_t lane, size_t lap) const;
};

#endif
