import json
import socket
import sys
from car import Car
from track import Track
import math


class NoobBot(object):

    def __init__(self, socket, name, key, command=None, trackname=None, password=None, carcount=None):
        self.command = command
        self.trackname = trackname
        self.password = password
        self.carcount = carcount
        self.socket = socket
        self.name = name
        self.key = key
        self.car = None
        self.color = None
        self.sim_log = None
        self.dt = 0
        self.t = 0
        self.track = None

        config_file = open('evan/config.json')
        self.config = json.load(config_file)
        config_file.close()

        for track in self.config['tracks']:
            if track['id'] == self.config['track']:
                self.track_config = track

        sim_filename = trackname+'.csv' if trackname is not None else 'sim.csv'
        self.sim_log = open(sim_filename, 'w')
        self.sim_log.write('Tick,Velocity,Acceleration,Angle,dAngle,Curvature,SampleWidth,Turbo\n')

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        if self.command is not None and self.command == "join":
            print "JOIN"
            return self.msg("joinRace", {'botId':{"name": self.name, "key": self.key},'trackName':self.trackname,'password':self.password,'carCount':self.carcount})
        elif command is not None and command == "create":
            print "CREATE"
            return self.msg("createRace", {'botId':{"name": self.name, "key": self.key},'trackName':self.trackname,'password':self.password,'carCount':int(self.carcount)})
        else:
            print "JOIN QUICKRACE"
            return self.msg("join", {"name": self.name, "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def activate_turbo(self):
        self.car.activate_turbo()
        self.msg("turbo", "booyeah")

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def make_switch(self, direction):
        print "switchLane: ", direction
        self.msg("switchLane", direction)

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        for car_data in data:
            if car_data['id']['color'] == self.color:
                self.car.update_vitals(car_data, self.t, self.dt)

        throttle = self.car.get_throttle()
        if self.car.wants_switch():
            self.make_switch(self.car.get_switch_direction())
        elif self.car.is_turbo_activatable():
            self.activate_turbo()
        else:
            self.throttle(throttle)

        if self.sim_log is not None:
            curvature = self.car.track.sample_curvature(1, 1, self.car.i_piece, self.car.x_piece, self.car.start_lane)
            sample_width = self.car.get_sample_width()
            self.sim_log.write(",".join([str(self.t),str(self.car.v),str(self.car.a),str(self.car.angle),str(self.car.d_angle),str(curvature),str(sample_width),str(self.car.get_turbo_factor())]) + '\n')

    def on_crash(self, data):
        print("{0} crashed.".format(data['name']))
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        for best_lap in data['bestLaps']:
            if best_lap['car']['color'] == self.color:
                print("Our best lap: {1} ({2}s)".format(best_lap['car']['name'], best_lap['result']['lap']+1, best_lap['result']['millis'] / 1000.0))
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
        print ("gameInit")
        self.track = Track(data['race']['track'], self.track_config['ideal_lanes'])
        for car in data['race']['cars']:
            if car['id']['color'] == self.color:
                self.car = Car(car['id']['name'], car['id']['color'], car['dimensions'], self.track)
        self.ping()

    def on_your_car(self, data):
        self.color = data['color']
        print ("{0} is {1}.".format(data['name'], data['color']))
        self.ping()

    def on_lap_finished(self, data):
        print ("lapFinished")
        self.ping()

    def on_turbo_available(self, data):
        print ("turboAvailable")
        self.car.enable_turbo(data['turboDurationTicks'], data['turboFactor'])
        self.ping()

    def update_tick(self, tick):
        self.dt = tick - self.t
        self.t = tick

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'yourCar': self.on_your_car,
            'lapFinished': self.on_lap_finished,
            'turboAvailable': self.on_turbo_available
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)

            if msg.get('gameTick'):
                self.update_tick( msg['gameTick'] )

            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) < 5 or len(sys.argv) == 6 or len(sys.argv) == 7 or len(sys.argv) > 9:
        print("Usage: ./run host port botname botkey [create|join trackname password carcount]")
    else:
        host, port, name, key = sys.argv[1:5]
        command, trackname, password, carcount = [None, None, None, None]
        if len(sys.argv) == 9:
            command, trackname, password, carcount = sys.argv[5:9]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key, command, trackname, password, carcount)
        bot.run()
