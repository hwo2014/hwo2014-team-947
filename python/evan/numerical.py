import math

#interpolates between [a,b] by alpha
def lerp(a, b, alpha):
    return alpha*b + (1-alpha)*a

#interpolates points on precomputed function fn, mapping it to the interval [a,b] and interpolating to find fn(x), anywhere that x is in [a,b]
def lerp_fn(fn, a, b, x):
    if x == a: return fn[0] 
    if x == b: return fn[len(fn)-1]
    if x < a or x > b: return None 

    step = (b - a) / (len(fn)-1)
    left = int(math.floor((x - a) / step))
    alpha = ((x - a) - left*step) / step
    return lerp(fn[left], fn[left+1], alpha)

#solves by finding the x producing the desired y, interpolating between samples
def solve_fn_by_y(fn, a, b, y):
    step = (b - a) / (len(fn)-1)
    for i in range(len(fn)-1):
        if fn[i] < y and y < fn[i+1]:
            slope = (fn[i+1] - fn[i]) / step
            return a + i * step + (y - fn[i]) / slope
    return None

#   f_k | initial value of f
# f_p_k | initial value of f'
# f_p_p | function f''
# [a,b] | interval
#     n | number of samples >= 2
#     y | solve for value
# neg_y | check -y as well
def solve_2nd_ode_for_x(f_k, f_p_k, f_p_p, f_u, a, b, n, y, neg_y=False, log=None):
    step = (b - a) / (n-1)
    x_0 = None
    y_0 = None
    x_1 = a
    y_1 = f_k
    f_p_p_k = f_p_p()
    if log: log.write(','.join([str(0),str(f_k),str(f_p_k),str(f_p_p_k)]) + '\n')
    for i in range(1,n):
        f_k += step*f_p_k
        f_p_p_k = f_p_p()
        f_p_k += step*f_p_p_k
        f_u(f_k, f_p_k)
        if log: log.write(','.join([str(i*step),str(f_k),str(f_p_k),str(f_p_p_k)]) + '\n')
        x_0 = x_1
        y_0 = y_1
        x_1 += step
        y_1 = f_k
        if (y_0 > y and y > y_1) or (y_0 < y and y < y_1):
            slope = (y_1 - y_0) / step
            return x_0 + (y - y_0) / slope
        elif y == y_0: return x_0
        elif y == y_1: return x_1

        if neg_y:
            minus_y = -y
            if (y_0 > minus_y and minus_y > y_1) or (y_0 < minus_y and minus_y < y_1):
                slope = (y_1 - y_0) / step
                return x_0 + (minus_y - y_0) / slope
            elif minus_y == y_0: return x_0
            elif minus_y == y_1: return x_1
    return None
